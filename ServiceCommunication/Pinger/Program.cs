﻿using System;
using RabbitMQ.Wrapper;

namespace Pinger
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "ping";
            string hostName = "localhost";
            string publisherQueueName = "pong_queue";
            string consumerQueueName = "ping_queue";
            string routingKey = "pong_queue";
            int delay = 2500;

            RabbitMQPublisher publisher;
            RabbitMQConsumer consumer;

            try
            {
                publisher = new RabbitMQPublisher(hostName, publisherQueueName, routingKey);
                consumer = new RabbitMQConsumer(hostName, consumerQueueName, routingKey);
                consumer.ListenQueue(publisher, message, delay);
                publisher.SendMessageToQueue(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return;
            }

            Console.WriteLine("Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}

