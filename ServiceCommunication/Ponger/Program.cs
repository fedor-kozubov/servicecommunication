﻿using System;
using RabbitMQ.Wrapper;

namespace Ponger
{
    class Program
    {
        static void Main(string[] args)
        {
            string message = "pong";
            string hostName = "localhost";
            string publisherQueueName = "ping_queue";
            string consumerQueueName = "pong_queue";
            string routingKey = "ping_queue";
            int delay = 2500;

            RabbitMQPublisher publisher;
            RabbitMQConsumer consumer;

            try
            {
                publisher = new RabbitMQPublisher(hostName, publisherQueueName, routingKey);
                consumer = new RabbitMQConsumer(hostName, consumerQueueName, routingKey);
                consumer.ListenQueue(publisher, message, delay);
                //publisher.SendMessageToQueue(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Exception: {ex.Message}");
                return;
            }

            Console.WriteLine("Press [enter] to exit.");
            Console.ReadLine();
        }
    }
}
