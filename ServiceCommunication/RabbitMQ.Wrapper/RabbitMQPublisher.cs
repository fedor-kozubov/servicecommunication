﻿using System;
using System.Text;
using RabbitMQ.Client;


namespace RabbitMQ.Wrapper
{
    public class RabbitMQPublisher : RabbitMQ
    {
        public RabbitMQPublisher(string hostName, string queueName, string routingKey) : base(hostName, queueName, routingKey)
        {
        }

        public void SendMessageToQueue(string message)
        {
            var body = Encoding.UTF8.GetBytes(message);
            channel.BasicPublish(exchange: "", routingKey: routingKey, basicProperties: null, body: body);
        }
    }
}
