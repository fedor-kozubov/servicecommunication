﻿using System;
using System.Text;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System.Threading;
using System.Threading.Tasks;

namespace RabbitMQ.Wrapper
{
    public class RabbitMQConsumer : RabbitMQ
    {
        public delegate void MessageReceivedDlg(string message);

        public RabbitMQConsumer(string hostName, string queueName, string routingKey) : base(hostName, queueName, routingKey)
        {
        }

        public void ListenQueue(RabbitMQPublisher publisher, string sendMessage, int delay)
        {
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += (model, ea) =>
            {
                var body = ea.Body.ToArray();
                var message = Encoding.UTF8.GetString(body);

                Console.WriteLine($"{DateTime.Now} {message}");

                Task tsk = new Task(() => {
                    Thread.Sleep(delay);
                    publisher.SendMessageToQueue(sendMessage);
                });
                tsk.Start();

            };
            channel.BasicConsume(queue: queueName, autoAck: true, consumer: consumer);
        }
    }
}