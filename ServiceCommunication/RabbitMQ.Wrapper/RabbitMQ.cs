﻿using System;
using RabbitMQ.Client;


namespace RabbitMQ.Wrapper
{
    public class RabbitMQ : IDisposable
    {
        protected readonly string queueName;
        protected readonly string routingKey;
        private readonly ConnectionFactory factory;
        private readonly IConnection connection;
        protected readonly IModel channel;

        public RabbitMQ(string hostName, string queueName, string routingKey)
        {
            this.queueName = queueName;
            this.routingKey = routingKey;

            factory = new ConnectionFactory() { HostName = hostName };
            connection = factory.CreateConnection();
            channel = connection.CreateModel();
            channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);
        }

        public void Dispose()
        {
            if (channel != null)
            {
                channel.Close();
            }
            if (connection != null)
            {
                connection.Close();
            }
        }
    }
}



